/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1;

import java.util.Scanner;


/**
 *13
 * @author JAVA
 */
public class Ejercicio1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int numero;
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese numero");
        numero = sc.nextInt();
        System.out.println("Valor numero = " + numero);
        if(numero < 10) {
            System.out.println("Numero de un digito");
        } else if (numero < 100) {
            System.out.println("Numero de dos digitos");
        } else if (numero < 1000) {
            System.out.println("Numero de tres digitos");
        } else {
            System.out.println("Numero de cuatro digitos");
        }
    }
    
}
