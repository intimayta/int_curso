/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio7;

import java.util.Scanner;

/**
 *
 * @author JAVA
 */
public class Ejercicio7 {

    public static void main(String[] args) {
        int n, c = 0, s = 1;
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese un numero");
        n = sc.nextInt();

        do {
            if (c != 0) {
                s = s + c;
                System.out.print(s);
                if (c != n - 1) {
                    System.out.print(", ");
                }
            }
            c++;
        } while (c < n);
        System.out.println("");
    }
}
