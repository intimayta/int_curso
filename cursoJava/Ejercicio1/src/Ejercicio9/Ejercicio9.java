/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio9;

import java.util.Scanner;

/**
 *
 * @author JAVA
 */
public class Ejercicio9 {

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int numero,fibo1,fibo2,i,res;
        do{
            System.out.print("Introduce numero : ");
            numero = sc.nextInt();
        }while(numero<=1);
        System.out.println("Los " + numero + " primeros términos de la serie de Fibonacci son:"); 

        fibo1=1;
        fibo2=1; 

        System.out.print(fibo1 + "+");
        for(i=2;i<=numero;i++){
             System.out.print(fibo2 + "+");
             fibo2 = fibo1 + fibo2;
             fibo1 = fibo2 - fibo1;
             
        }
        res=fibo1+fibo2-1;
        System.out.println();
        System.out.println("El resultado es " + res);
    }
}
